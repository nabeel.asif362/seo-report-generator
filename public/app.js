const loader = document.querySelector('#loading');
const runSeoAnalysisBtn = document.getElementById('run-seo-btn');

function toggleLoading(display) {
  loader.classList.toggle('display', display);
}

runSeoAnalysisBtn.addEventListener('click', async () => {
  const input = document.getElementById('csv-input');

  if (!input.files.length) {
    alert('Please select a file before running the analysis.');
    return;
  }

  const data = new FormData();
  data.append('file', input.files[0]);

  toggleLoading(true);

  try {
    const response = await axios.post('/run-analysis', data, { responseType: 'blob' });
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'seo-metrics.zip');
    document.body.appendChild(link);
    link.click();
    link.remove();
  } catch (error) {
    console.error(error);
    alert('Error occurred while processing your request.');
  } finally {
    toggleLoading(false);
  }
});
