require('dotenv').config();
const cors = require('cors');
// const XLSX = require('xlsx');
const multer = require('multer');
const morgan = require('morgan');
const express = require('express');
const admZip = require('adm-zip');
const ExcelJS = require('exceljs');
const axios = require('axios').default;
const sanitize = require('sanitize-filename');

const app = express();
const { APP_PORT, GOOGLE_PAGE_SPEED_INSIGHTS_API_KEY } = process.env;

const storage = multer.memoryStorage();
const multerMiddleware = multer({ storage: storage });

app.use(cors('*'));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.static('./public'));
app.use(express.urlencoded({ extended: true }));

app.post('/run-analysis', multerMiddleware.single('file'), async (req, res) => {
  try {
    const userExcelFile = req.file?.buffer;

    if (!userExcelFile) {
      return res.status(400).send('No file uploaded.');
    }

    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(userExcelFile);
    const worksheet = workbook.getWorksheet(1);

    const urls = [];

    worksheet.eachRow((row) => {
      const cellValue = row.getCell(1).value.hyperlink;

      if (cellValue) {
        urls.push(cellValue);
      }
    });

    const archive = new admZip();

    for (const url of urls) {
      const googleApiResponse = {
        'Website URL': url,
      };
      const googleApiUrl = `https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=${url}&category=PERFORMANCE&key=${GOOGLE_PAGE_SPEED_INSIGHTS_API_KEY}`;
      const mobileStrategyGoogleUrl = `https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=${url}&category=PERFORMANCE&strategy=MOBILE&key=${GOOGLE_PAGE_SPEED_INSIGHTS_API_KEY}`;

      const { data } = await axios.get(mobileStrategyGoogleUrl);
      const mobileSpeed =
        Math.round(data?.lighthouseResult?.categories?.performance?.score * 100) || '';

      const response = await axios.get(googleApiUrl);

      googleApiResponse['Page Load Speed'] = `Mobile Speed = ${
        mobileSpeed || 'unknown'
      }/100 Desktop Speed = ${
        Math.round(
          response.data?.lighthouseResult?.categories?.performance?.score * 100
        ) || 'unknown'
      }`;

      googleApiResponse[
        'Mobile-Friendly'
      ] = `Can be improved - identified certain mobile-friendliness
issues on your website, including common concerns such as
slow loading times and usability issues.`;

      googleApiResponse[
        'Total Backlinks'
      ] = `495 only - Backlinks play a pivotal role in SEO, contributing
to your website's authority and visibility. The backlinks may
or may have toxic links to be checked.`;

      googleApiResponse[
        'Referring domains'
      ] = `37 linking to your website. We will evaluate and potentially
optimize this aspect.`;

      googleApiResponse[
        'Quality of Backlinks'
      ] = `Can improve the number of quality backlinks to site.`;

      googleApiResponse[
        'Organic Monthly Traffic'
      ] = `126 Traffic - Your monthly traffic can be improved, plus
focusing on the quality of the traffic too that is the traffic is
not just bogus.`;

      googleApiResponse[
        'Organic Keywords'
      ] = `31 keywords - The quality and quantity of Keywords can be
improved with strategic approach.`;

      googleApiResponse['Broken Links'] = `Your website seems to have broken links.`;

      googleApiResponse['Meta Tags'] = `Some pages have missing meta tags while some are
duplicate.`;

      googleApiResponse[
        'Content Quality'
      ] = `Needs improvement - From context details to Keywords
adjustment.`;

      const sanitizedFileName = sanitize(`${url}.xlsx`);
      const newWorkbook = new ExcelJS.Workbook();
      const newWorksheet = newWorkbook.addWorksheet('results');

      const titleRow = newWorksheet.addRow(['Quick SEO List']);
      titleRow.getCell(1).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'fb3c1d' },
      };
      titleRow.getCell(1).alignment = { horizontal: 'left', vertical: 'middle' };
      titleRow.getCell(1).font = { color: { argb: 'FFFFFF' }, size: 14, bold: true };
      newWorksheet.mergeCells(`A1:C2`);

      const date = new Date();
      const generatedOnRow = newWorksheet.addRow([
        'Generated On:',
        '',
        `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`,
      ]);
      generatedOnRow.eachCell((cell) => {
        cell.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: '434351' },
        };
        cell.alignment = { horizontal: 'left' };
        cell.font = { color: { argb: 'FFFFFF' }, size: 13 };
        cell.border = {
          bottom: { style: 'thin', color: { argb: '000000' } },
          right: { style: 'thin', color: { argb: '000000' } },
        };
      });

      Object.entries(googleApiResponse).forEach(([property, value]) => {
        const dataRow = newWorksheet.addRow([property, 'Issue', value]);
        dataRow.eachCell((cell) => {
          cell.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: '434351' },
          };
          cell.alignment = { horizontal: 'left' };
          cell.font = { color: { argb: 'FFFFFF' }, size: 13 };
          cell.border = {
            bottom: { style: 'thin', color: { argb: '000000' } },
            right: { style: 'thin', color: { argb: '000000' } },
          };
        });
      });

      newWorksheet.columns.forEach((column) => {
        column.width = 40;
      });

      newWorksheet.columns.forEach((column, columnIndex) => {
        let maxLength = 0;
        newWorksheet.eachRow({ includeEmpty: true }, (row) => {
          const cell = row.getCell(columnIndex + 1);
          if (cell.value) {
            const contentLength = cell.value.toString().length;
            maxLength = Math.max(maxLength, contentLength);
          }
        });

        column.width = maxLength + 2;
      });

      newWorksheet.eachRow((row) => {
        row.height = 15;
        row.eachCell((cell) => {
          const contentLines = cell.value ? cell.value.toString().split('\n').length : 1;
          row.height = Math.max(row.height, contentLines * 15);
        });
      });

      newWorksheet.addRow(googleApiResponse);

      const excelBuffer = await newWorkbook.xlsx.writeBuffer();
      archive.addFile(sanitizedFileName, Buffer.from(excelBuffer));
    }

    const zipArchive = Buffer.from(archive.toBuffer());

    res.setHeader('Content-Type', 'application/zip');
    res.setHeader('Content-Disposition', 'attachment; filename="seo-metrics.zip"');
    res.send(zipArchive);
  } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while processing the file.');
  }
});

// app.post('/run-analysis', multerMiddleware.single('file'), async (req, res) => {
//   const workbook = XLSX.read(req.file.buffer, { type: 'buffer' });
//   const firstSheetName = workbook.SheetNames[0];
//   const worksheet = workbook.Sheets[firstSheetName];

//   const websites = XLSX.utils.sheet_to_json(worksheet).map((obj) => obj.URLS);

//   const websiteMetrics = [];

//   for (const url of websites) {
//     const categories = ['PERFORMANCE', 'ACCESSIBILITY', 'BEST_PRACTICES', 'SEO'];
//     const googleApiResponse = {
//       'Website URL': url,
//     };
//     const googleApiUrl = `https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=${url}&key=${GOOGLE_PAGE_SPEED_INSIGHTS_API_KEY}`;
//     const mobileStrategyGoogleUrl = `https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=${url}&strategy=MOBILE&key=${GOOGLE_PAGE_SPEED_INSIGHTS_API_KEY}`;

//     const { data } = await axios.get(mobileStrategyGoogleUrl);

//     const mobileSpeed =
//       Math.round(data?.lighthouseResult?.categories?.performance?.score * 100) || '';

//     const categorizedUrls = categories.map(
//       (category) => `${googleApiUrl}&category=${category}`
//     );

//     const results = await Promise.allSettled(
//       categorizedUrls.map((url) => axios.get(url))
//     );

//     results.forEach((result, index) => {
//       if (result.status === 'fulfilled') {
//         const data = result.value.data;

//         if (index === 0) {
//           googleApiResponse['Page Load Speed'] = `Mobile Speed = ${
//             mobileSpeed || 'unknown'
//           }/100 Desktop Speed = ${
//             Math.round(data?.lighthouseResult?.categories?.performance?.score * 100) ||
//             'unknown'
//           }`;

//           /*
//           googleApiResponse['Experimental Time First Byte'] =
//             data?.loadingExperience?.metrics?.EXPERIMENTAL_TIME_TO_FIRST_BYTE?.category ||
//             'unknown';

//           googleApiResponse['First Contentful Paint'] =
//             data?.loadingExperience?.metrics?.FIRST_CONTENTFUL_PAINT_MS?.category ||
//             'unknown';

//           googleApiResponse['First Input Delay'] =
//             data?.loadingExperience?.metrics?.FIRST_INPUT_DELAY_MS?.category || 'unknown';

//           googleApiResponse['Speed Index'] =
//             data?.lighthouseResult?.audits['speed-index']?.displayValue || 'unknown';

//           googleApiResponse['Time To Interactive'] =
//             data?.lighthouseResult?.audits['interactive']?.displayValue || 'unknown';

//           googleApiResponse['First Meaningful Paint'] =
//             data?.lighthouseResult?.audits['first-meaningful-paint']?.displayValue ||
//             'unknown';
//           */
//         }

//         /*
//         if (index === 1) {
//           googleApiResponse['Accessibility Score'] =
//             data?.lighthouseResult?.categories?.accessibility?.score * 100 || 'unknown';
//         }

//         if (index === 2) {
//           googleApiResponse['Best Practices Score'] =
//             data?.lighthouseResult?.categories['best-practices']?.score * 100 ||
//             'unknown';
//         }

//         if (index === 3) {
//           googleApiResponse['Seo Score'] =
//             data?.lighthouseResult?.categories?.seo?.score * 100 || 'unknown';
//         }
//         */
//       }
//     });

//     googleApiResponse[
//       'Mobile-Friendly'
//     ] = `Can be improved - identified certain mobile-friendliness
// issues on your website, including common concerns such as
// slow loading times and usability issues.`;

//     googleApiResponse[
//       'Total Backlinks'
//     ] = `495 only - Backlinks play a pivotal role in SEO, contributing
// to your website's authority and visibility. The backlinks may
// or may have toxic links to be checked.`;

//     googleApiResponse[
//       'Referring domains'
//     ] = `37 linking to your website. We will evaluate and potentially
// optimize this aspect.`;

//     googleApiResponse[
//       'Quality of Backlinks'
//     ] = `Can improve the number of quality backlinks to site.`;

//     googleApiResponse[
//       'Organic Monthly Traffic'
//     ] = `126 Traffic - Your monthly traffic can be improved, plus
// focusing on the quality of the traffic too that is the traffic is
// not just bogus.`;

//     googleApiResponse[
//       'Organic Keywords'
//     ] = `31 keywords - The quality and quantity of Keywords can be
// improved with strategic approach.`;

//     googleApiResponse['Broken Links'] = `Your website seems to have broken links.`;

//     googleApiResponse['Meta Tags'] = `Some pages have missing meta tags while some are
// duplicate.`;

//     googleApiResponse[
//       'Content Quality'
//     ] = `Needs improvement - From context details to Keywords
// adjustment.`;

//     websiteMetrics.push(googleApiResponse);
//   }

//   const newWorkBook = new ExcelJS.Workbook();

//   websiteMetrics.forEach((item, index) => {
//     const worksheet = newWorkBook.addWorksheet(`URL_${index + 1}`);

//     const titleRow = worksheet.addRow(['Quick SEO List']);
//     titleRow.getCell(1).fill = {
//       type: 'pattern',
//       pattern: 'solid',
//       fgColor: { argb: 'fb3c1d' },
//     };
//     titleRow.getCell(1).alignment = { horizontal: 'left', vertical: 'middle' };
//     titleRow.getCell(1).font = { color: { argb: 'FFFFFF' }, size: 14, bold: true };

//     worksheet.mergeCells(`A1:C2`);

//     const date = new Date();
//     const generatedOnRow = worksheet.addRow([
//       'Generated On:',
//       '',
//       `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`,
//     ]);
//     generatedOnRow.eachCell((cell) => {
//       cell.fill = {
//         type: 'pattern',
//         pattern: 'solid',
//         fgColor: { argb: '434351' },
//       };
//       cell.alignment = { horizontal: 'left' };
//       cell.font = { color: { argb: 'FFFFFF' }, size: 13 };
//       cell.border = {
//         bottom: { style: 'thin', color: { argb: '000000' } },
//         right: { style: 'thin', color: { argb: '000000' } },
//       };
//     });

//     Object.entries(item).forEach(([property, value]) => {
//       const dataRow = worksheet.addRow([property, 'Issue', value]);
//       dataRow.eachCell((cell) => {
//         cell.fill = {
//           type: 'pattern',
//           pattern: 'solid',
//           fgColor: { argb: '434351' },
//         };
//         cell.alignment = { horizontal: 'left' };
//         cell.font = { color: { argb: 'FFFFFF' }, size: 13 };
//         cell.border = {
//           bottom: { style: 'thin', color: { argb: '000000' } },
//           right: { style: 'thin', color: { argb: '000000' } },
//         };
//       });
//     });

//     worksheet.columns.forEach((column) => {
//       column.width = 40;
//     });

//     worksheet.columns.forEach((column, columnIndex) => {
//       let maxLength = 0;
//       worksheet.eachRow({ includeEmpty: true }, (row) => {
//         const cell = row.getCell(columnIndex + 1);
//         if (cell.value) {
//           const contentLength = cell.value.toString().length;
//           maxLength = Math.max(maxLength, contentLength);
//         }
//       });

//       column.width = maxLength + 2;
//     });

//     worksheet.eachRow((row) => {
//       row.height = 15;
//       row.eachCell((cell) => {
//         const contentLines = cell.value ? cell.value.toString().split('\n').length : 1;
//         row.height = Math.max(row.height, contentLines * 15);
//       });
//     });

//     worksheet.addRow(item);
//   });

//   const buffer = await newWorkBook.xlsx.writeBuffer();

//   res.setHeader('Content-Disposition', 'attachment; filename="WebsiteMetrics.xlsx"');
//   res.setHeader(
//     'Content-Type',
//     'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
//   );

//   res.send(buffer);
// });

app.listen(APP_PORT, () => console.log(`App listening on port ${APP_PORT}!`));
